module.exports = function (grunt) {
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        /*configurando la herramienta de sass*/
        sass: {
            /*segun la distribucion en mi caso la carpeta de districion es dist*/
            dist: {
                /*en todos los archivos*/
                files: [{
                    expand: 'true',
                    cwd: 'css',
                    /*dentro de la carpeta css*/
                    src: ['*.scss'],
                    /*que tengan extencion scss*/
                    dest: 'css',
                    /*los mande como destinos a la carpeta css*/
                    ext: '.css' /*y les ponga la extencion css*/

                }]
            }
        },
        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                }
            },
            options: {
                watchTask: true,
                server: {
                    baseDir: './'
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'imagenes/*.{png,jpg,gif,jpeg}',
                    dest: 'dist'

                }]

            }
        },
        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './', //current working directory
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'node-modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
               }]
            }
        },
        clean: {
            build: {
                src: ['dist/'] //clean the distribution folder

            }
        },
        cssmin: {
            dist: {}
        },
        uglify: {
            dist: {}
        },
        filerev: {
            options: {
                algorithm: 'md5',
                length: 15
            },
            files: {
                src: ['dist/css/*.css', 'dist/js/*.js']

            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },
        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function (context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false

                                }

                            }
                        }]
                    }
                }
            }
        },
        usemin: {
            html: ['dist/index.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']

            }

        }
    })
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('imagen', ['imagemin']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('build', [
     'clean', //Borramos el contenido de dist
     'copy', //Copiamos los archivos html a dist
     'imagemin', //Optimizamos imagenes y las copiamos a dist
     'useminPrepare', //Preparamos la configuracion de usemin
     'concat',
     'cssmin',
     'uglify',
     'filerev', //Agregamos cadena aleatoria
     'usemin' //Reemplazamos las referencias por los archivos generados por filerev
     ])
}
