$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#exampleModal').on("show.bs.modal", function (e) {
        console.log("el model se esta mostrando");
        $('#contactoBtn').addClass("btn-danger")
        $('#contactoBtn').prop("disabled", true) //quita las propiedades
    })
    $('#exampleModal').on("shown.bs.modal", function (e) {
        console.log("el model se esta mostrando");
    })
    $('#exampleModal').on("hide.bs.modal", function (e) {
        console.log("el model se oculto");
        $('#contactoBtn').removeClass("btn-danger")
        $('#contactoBtn').prop("disabled", false) //quita las propiedades

    })
    $('#exampleModal').on("hidden.bs.modal.bs.modal", function (e) {
        console.log("el model se oculto hace unos segundos");

    })
})
